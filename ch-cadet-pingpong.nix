{ mkDerivation, base, distributed-process, network-transport
, network-transport-cadet, stdenv
}:
mkDerivation {
  pname = "ch-cadet-pingpong";
  version = "0.0.0.1";
  src = ./.;
  isLibrary = false;
  isExecutable = true;
  executableHaskellDepends = [
    base distributed-process network-transport network-transport-cadet
  ];
  license = stdenv.lib.licenses.agpl3;
}
