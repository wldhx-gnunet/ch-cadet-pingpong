module Main where

import Control.Concurrent (threadDelay)
import Control.Monad (forever, void)
import qualified Control.Exception as E
import qualified Data.ByteString.Char8 as C
import Control.Distributed.Process
import Control.Distributed.Process.Node
import Network.Transport
import Network.Transport.CADET

-- from distributed-process-p2p
-- | Make a NodeId from "host:port" string.
makeNodeId :: String -> NodeId
makeNodeId = NodeId . EndPointAddress . C.pack

main :: IO ()
main = run $ makeNodeId "TJWQ404E0EHFC48KCRD4BVGARWEAMYF0XG822SK3RJHKRXQT0N80:q"

run other = do
  let params = CADETParameters { configPath = "gnunet2.conf" }
  E.bracket (createTransport params) closeTransport $ \t -> do
    E.bracket (newLocalNode t initRemoteTable) closeLocalNode $ \node -> runProcess node $ do
      self <- getSelfPid
      register "tstSrv" self
      say $ show other
      void . spawnLocal $ forever $ do
        -- Without the following delay, the process sometimes exits before the messages are exchanged.
        liftIO $ threadDelay 200000
        nsendRemote other "tstSrv" ("mow" :: String)
      forever $ do
        m <- expectTimeout 1000000 :: Process (Maybe String)
        say $ show m
